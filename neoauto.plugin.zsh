htdocs_dir=$HTDOCS_DIR
neoauto_base_dir=$HTDOCS_DIR/apps/orbis/neoauto

#ids de usuario y grupo
dev_uid=$(id -u)
dev_gid=$(id -g)

function frontend_gulp_task()
{
    neoauto_dir=$neoauto_base_dir/neoauto3.com
    src_default_dir=$neoauto_dir/public/neoauto3
    flux_default_dir=$src_default_dir/frontend/neoauto3
    gulpfile_default_path='frontend/neoauto3/gulpfile.js'

    var_config=""
    var_frontend_img="frontend_img"
    var_task=""

    while getopts ":ci:t:" opt
    do
        case $opt in
            c) var_config="true" ;;
            i) var_frontend_img=$OPTARG ;;
            t) var_task=$OPTARG ;;
        esac
    done

    shift $(($OPTIND - 1))

    src_dir=$src_default_dir
    flux_dir=$flux_default_dir
    gulpfile_path=$gulpfile_default_path
    
    if [ ! -z $var_config ]; then
        echo $'\033[1;32mIngrese el directorio base: \033[1;31m[defecto: '"${src_default_dir}"$']\033[0m'
        read src_dir
        src_dir=${src_dir:-$src_default_dir}

        echo $'\033[1;32mIngrese el directorio del proyecto frontend: \033[1;31m[defecto: '"${flux_default_dir}"$']\033[0m'
        read flux_dir
        flux_dir=${flux_dir:-$flux_default_dir}

        echo $'\033[1;32mIngrese la ruta del gulpfile.js relativa al directorio base: \033[1;31m[defecto: '"${gulpfile_default_path}"$']\033[0m'
        read gulpfile_path
        gulpfile_path=${gulpfile_path:-$gulpfile_default_path}
    fi

    #homedir de docker
    dockerfront_dir="/usr/local/share"

    if [ -z $var_task ]; then
        docker run --rm -e DEV_UID=$dev_uid -e DEV_GID=$dev_gid -v $src_dir:$dockerfront_dir/src -v $flux_dir:$dockerfront_dir/flux $var_frontend_img gulp --gulpfile $gulpfile_path
    else
        docker run --rm -e DEV_UID=$dev_uid -e DEV_GID=$dev_gid -v $src_dir:$dockerfront_dir/src -v $flux_dir:$dockerfront_dir/flux $var_frontend_img gulp $var_task --gulpfile $gulpfile_path
    fi
}

function bower_install_task() {
    #directorios por defecto
    neoauto_dir=$neoauto_base_dir/neoauto3.com
    src_default_dir=$neoauto_dir/public/neoauto3/frontend/neoauto3
    dest_default_dir=$neoauto_dir/public/neoauto3/public/static/neoauto3/js/dist/libs

    need_config=$1

    case "$need_config" in
        -c|--config)
        echo $'\033[1;32mIngrese el directorio base: \033[1;31m[defecto: '"${src_default_dir}"$']\033[0m'
        read src_dir
        src_dir=${src_dir:-$src_default_dir}

        echo $'\033[1;32mIngrese el directorio en donde se instalarán las librerías js: \033[1;31m[defecto: '"${dest_default_dir}"$']\033[0m'
        read dest_dir
        dest_dir=${dest_dir:-$dest_default_dir}
        ;;

        *)
        src_dir=$src_default_dir
        dest_dir=$dest_default_dir
        ;;
    esac

    docker run --rm -v $src_dir:/base -v $dest_dir:/dest bower_img bower install --allow-root --config.directory=/dest
}

function composer_base_task() {
    params="$@"

    if [ -z $params ]; then
        docker run --rm -v $(pwd):/base composer_img composer --ignore-platform-reqs
    else
        docker run --rm -v $(pwd):/base composer_img composer $params --ignore-platform-reqs
    fi
}

function composer_install_task() {
    docker run --rm -v $(pwd):/base composer_img composer install --ignore-platform-reqs
}

function composer_require_task() {
    package="$1"

    if [ -z $package ]; then
        echo "se necesita especificar el paquete a instalar"
    else
        docker run --rm -v $(pwd):/base composer_img composer $package --ignore-platform-reqs
    fi
}

function composer_update_task() {
    package="$1"

    if [ -z $package ]; then
        docker run --rm -v $(pwd):/base composer_img composer update --ignore-platform-reqs
    else
        docker run --rm -v $(pwd):/base composer_img composer update $package --ignore-platform-reqs
    fi
}

